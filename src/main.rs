use async_std::task::block_on;
mod webserver;

fn main() {
    block_on(webserver::run());
}
