use serde::Deserialize;
use sqlite::ConnectionWithFullMutex;
use tide::{log, Request};
mod litedb;

struct TideState {
    db_conn: ConnectionWithFullMutex,
}

impl Clone for TideState {
    fn clone(&self) -> TideState {
        self
    }
}

pub async fn run() {
    let tide_state = TideState {
        db_conn: litedb::init(),
    };
    let mut app = tide::with_state(tide_state);
    log::start();
    app.with(tide::log::LogMiddleware::new());
    app.at("/posty").post(posty);
    app.listen("::1:50000")
        .await
        .expect("I had a listening problem.")
}

#[derive(Debug, Deserialize)]
struct PostData {
    text: String,
}

async fn posty(mut req: tide::Request<TideState>) -> tide::Result {
    let PostData { text } = req.body_json().await?;
    log::info!("posty {}", text);
    Ok(format!("ok {}", text).into())
}
