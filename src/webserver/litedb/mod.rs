use sqlite::ConnectionWithFullMutex;

pub fn init() -> ConnectionWithFullMutex {
    let connection = sqlite::Connection::open_with_full_mutex("tmp/gnuplex.sqlite3")
        .expect("I had a problem opening tmp/gnuplex.sqlite3.");
    connection
        .execute("create table if not exists t (text text);")
        .expect("I had a problem initializing the DB.");
    connection
}
